import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SuggestionFormComponent } from './suggestion-form/suggestion-form.component';
import { ReactiveFormsModule } from '@angular/forms';

// angular material
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import {MatListModule } from '@angular/material/list';
import {MatDialogModule} from '@angular/material/dialog';

//firebase imports
import {AngularFireModule } from '@angular/fire';
import {AngularFirestoreModule } from '@angular/fire/firestore';
import firestoreConfig from '../../my-firestore';
import { SuggestionListComponent } from './suggestion-list/suggestion-list.component';
import { RestaurantResultComponent } from './restaurant-result/restaurant-result.component';
import { ModalComponent } from './modal/modal.component';

@NgModule({
  declarations: [AppComponent, SuggestionFormComponent, SuggestionListComponent, RestaurantResultComponent, ModalComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatDialogModule,
    AngularFireModule.initializeApp(firestoreConfig),
    AngularFirestoreModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
