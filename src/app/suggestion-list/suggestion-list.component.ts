import { Component, OnInit } from '@angular/core';
import {RestaurantService} from '../services/restaurant.service';
import {Restaurant} from '../models/restaurant';
import {AngularFirestoreCollection} from '@angular/fire/firestore';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-suggestion-list',
  templateUrl: './suggestion-list.component.html',
  styleUrls: ['./suggestion-list.component.scss']
})
export class SuggestionListComponent implements OnInit {
  restaurantsCollection: AngularFirestoreCollection<Restaurant>;
  restaurants$: Observable<Restaurant[]>;

  constructor(private rs: RestaurantService) { }

 async ngOnInit() {
    this.restaurantsCollection = await this.rs.readResaturant();
    this.restaurants$ = this.restaurantsCollection.valueChanges( {idField : 'id'});
  }
  vote(restaurant) {
    console.log('restaurant.id', restaurant.id);
    this.rs.voteForRestaurant(restaurant);
  }
  setRankLabel(restaurant) {
    const label = restaurant.votes <= 1 ? `${restaurant.votes} vote pour ${restaurant.name}` : `${restaurant.votes} votes pour ${restaurant.name}`;
    return label;

}
}
