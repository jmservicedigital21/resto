import {Component, Inject, OnInit} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import {RestaurantService} from '../services/restaurant.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {


  constructor(public dialogRef: MatDialogRef<any>, @Inject (MAT_DIALOG_DATA) public data, private rs: RestaurantService  ) { }

  ngOnInit(): void {
  }
  onNoClick(){
    this.dialogRef.close('nope');
  }
  async delete(){
    const result = await this.rs.deleteRestaurant(this.data)
    //TODO delete restau
    this.dialogRef.close(result);
    console.log('restau from dialog', this.data);
  }
}
