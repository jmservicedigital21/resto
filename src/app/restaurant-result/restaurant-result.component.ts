import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { Restaurant } from '../models/restaurant';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ModalComponent} from '../modal/modal.component';

@Component({
  selector: 'app-restaurant-result',
  templateUrl: './restaurant-result.component.html',
  styleUrls: ['./restaurant-result.component.scss']
})
export class RestaurantResultComponent implements OnChanges {
  @Input() restaurants$;
  modalResult;
  sortedRestaurants: Restaurant[] = [];

  constructor(public dialog: MatDialog) { }

  openDialog(restau) {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '350px',
      data: {...restau}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.modalResult = result;
      console.log('openDialog / aftrClosed()', this.modalResult);
    });
  }

  ngOnChanges(changes): void {
    console.log('changes', changes);
    if (!changes.restaurants$.currentValue) {
      return;
    }
    changes.restaurants$.currentValue.pipe(
      map((restaurants: Restaurant[]) => {
        const sortResult = restaurants.sort(this.sortByScore);
        this.sortedRestaurants = sortResult;
      })
    ).subscribe();
  }
  sortByScore(a, b) {
        // a.votes > b.votes orders vote desc
      if (a.votes > b.votes) {
    return -1;
      }else{
        return 1
      }
      return 0
  }
  setRankLabel(restaurant) {
    const label = restaurant.votes <= 1 ? `${restaurant.votes} vote pour ${restaurant.name}` : `${restaurant.votes} votes pour ${restaurant.name}`;
    return label;

  }
  onSelectionChange(event) {
    console.log(event.option.value);
  }
  openConfirmDialog(restaurant) {
this.openDialog(restaurant);
  }
}
