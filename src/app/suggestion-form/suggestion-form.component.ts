import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, NgForm } from '@angular/forms';
import {RestaurantService} from '../services/restaurant.service';

@Component({
  selector: 'app-suggestion-form',
  templateUrl: './suggestion-form.component.html',
  styleUrls: ['./suggestion-form.component.scss'],
})
export class SuggestionFormComponent implements OnInit {
  suggestionForm;
message;
  constructor(private fb: FormBuilder, private rs: RestaurantService ) {}

  ngOnInit(): void {
    this.suggestionForm = this.fb.group({
      name: ['', Validators.required],
    });
  }

  async addRestaurant() {
    console.log(this.suggestionForm.value);
    const result = await this.rs.cretateRestaurant(this.suggestionForm.value.name);
    console.log('result', result);
    this.suggestionForm.reset();
  }

}
