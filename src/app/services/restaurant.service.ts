import { Injectable } from '@angular/core';
import {AngularFirestore } from '@angular/fire/firestore'
import {Restaurant} from '../models/restaurant';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  constructor(private afs: AngularFirestore) {}

cretateRestaurant(name) {
    return this.afs
      .collection('wte-restaurants')
      .add({
      name: name,
      createdAt: Date.now(),
      votes: 0
    });
}
readResaturant() {
 return this.afs.collection<Restaurant>('wte-restaurants', ref => ref.orderBy('name', 'asc'));
}
voteForRestaurant(restaurant) {
    return this.afs.doc(`wte-restaurants/${restaurant.id}`).update({
      ...restaurant,
      votes: restaurant.votes + 1
    });
}
deleteRestaurant(restaurant) {
    return this.afs.doc(`wte-restaurant/${restaurant.id}`).delete()
}
}
